import uuid
from django.db import models
from django_extensions.db.models import TimeStampedModel
from auction.users.models import User


class AuctionItem(TimeStampedModel):
    name = models.CharField(verbose_name='Please enter name of the item.', max_length=100, unique=False)
    description = models.TextField(verbose_name='Please enter description for item.')
    start_time = models.DateTimeField(verbose_name="Please enter the date and time when the auction starts.")
    end_time = models.DateTimeField(verbose_name="Please enter the date and time when the auction ends.")
    winner = models.ForeignKey(User, null=True, blank=True, on_delete=models.DO_NOTHING,
                               related_name='winning_user')
    image = models.URLField(verbose_name="Please enter a URL for image of the item.", null=True, blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE,
                              related_name='owner')
    minimum_bid = models.FloatField(default=1.0)
    uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)

    def __str__(self):
        return self.name

