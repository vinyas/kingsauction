import json
import uuid
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from django_celery_beat.models import PeriodicTask, PeriodicTasks, CrontabSchedule
from .models import AuctionItem
from .serializers import AuctionItemSerializer


def add_task_for_auction(auction_item):
    cron_job = CrontabSchedule.objects.create(day_of_month=auction_item.end_time.day,
                                              month_of_year=auction_item.end_time.month,
                                              hour=auction_item.end_time.hour,
                                              minute=auction_item.end_time.minute)

    periodic_task = PeriodicTask.objects.create(name=auction_item.name + str(uuid.uuid1()), crontab=cron_job,
                                                task='auction.auction_items.tasks.run_auction',
                                                )
    periodic_task.kwargs = json.dumps({'auction_uuid': str(auction_item.uuid),
                                       'task_id': periodic_task.id})
    periodic_task.save()
    return True


@login_required
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def add_auction_item(request):
    """
    {
        "owner_id" : "697579a1-2f66-4a9d-a107-e8248b7ea244",
        "name" : "Football",
        "description" : " Signed by Gerrard",
        "start_time": "2018-09-02 16:00:00",
        "end_time" : "2018-09-04 16:00:00"
    }
    :param request: 
    :return: Response object with status and error or success message
    """
    data = request.data
    if data['owner_id'] == str(request.user.uuid):
        auction_serializer = AuctionItemSerializer(data=data)
        if auction_serializer.is_valid():
            auction_object = auction_serializer.save()
            add_task_for_auction(auction_object)
            return Response({'message': 'Success'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': auction_serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    return Response({'message': 'Oops !!'}, status=status.HTTP_400_BAD_REQUEST)


def list_all_items(request):
    items = list(AuctionItem.objects.all().values().order_by('-created'))
    return render(request, 'all_items.html', context={'items': items})
