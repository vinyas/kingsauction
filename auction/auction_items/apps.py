from django.apps import AppConfig


class AuctionItemsConfig(AppConfig):
    name = 'auction.auction_items'
