from rest_framework import serializers
from auction.users.models import User
from .models import AuctionItem


class AuctionItemSerializer(serializers.ModelSerializer):
    winner_id = serializers.SlugRelatedField(slug_field='uuid', source='winner',
                                             queryset=User.objects.all(), required=False)
    owner_id = serializers.SlugRelatedField(slug_field='uuid', source='owner',
                                            queryset=User.objects.all())

    class Meta:
        model = AuctionItem
        fields = ('winner_id', 'owner_id', 'name', 'end_time', 'start_time', 'description', 'image')
