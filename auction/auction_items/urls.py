from django.urls import path

from .views import add_auction_item, list_all_items

app_name = "auction_items"
urlpatterns = [
    path("list/", view=list_all_items, name="list"),
    path("add-auction-item/", view=add_auction_item, name="add_item"),
]
