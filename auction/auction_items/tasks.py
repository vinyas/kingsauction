import logging
from ..taskapp.celery import app
from django_celery_beat.models import PeriodicTask, PeriodicTasks, CrontabSchedule
from auction.auction_items.models import AuctionItem
from auction.bids.models import Bid

logging.basicConfig(format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)
log = logging.getLogger(__name__)


@app.task(bind=True)
def run_auction(self, *args, **kwargs):
    print(self)
    try:
        item = AuctionItem.objects.get(uuid=kwargs.get('auction_uuid'))
    except AuctionItem.DoesNotExist as e:
        log.exception(e)
        item = None
        pass
    try:
        highest_bid = Bid.objects.filter(item=item).order_by('-amount').first()
        log.info(highest_bid)
        item.winner = highest_bid.bidder
        item.save()
    except Exception as e:
        log.exception(e)
        pass
    try:
        task = PeriodicTask.objects.get(id=kwargs.get('task_id'))
        task.enable = False
        task.save()
    except Exception as e:
        log.exception(e)
        pass

    return True
