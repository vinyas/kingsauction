import uuid
from django.db import models
from django_extensions.db.models import TimeStampedModel
from auction.auction_items.models import AuctionItem
from auction.users.models import User


class Bid(TimeStampedModel):
    item = models.ForeignKey(AuctionItem, on_delete=models.CASCADE)
    bidder = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.FloatField(verbose_name='Please enter the bid amount')
    uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)

    def __str__(self):
        return '---'.join([self.item.name, self.bidder.name])
