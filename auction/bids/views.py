import json
import logging
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from .models import Bid
from .serializers import BidSerializer


def show_all_bids(request):
    bids = Bid.objects.filter(bidder_id=request.user.id)
    return render(request, 'all_bids.html', context={'bids': bids})


@login_required
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def add_bid(request):
    """
    {
        "bidder_id" : "396b5cbd-3752-4e91-8ee8-baad13e2340c",
        "item_id": "03be636c-3779-4bdc-9889-18593bc8f96e",
        "amount": "12.45"
    }
    :param request:
    :return:
    """
    data = request.data
    bid_serializer = BidSerializer(data=data)
    if bid_serializer.is_valid():
        bid = bid_serializer.save()
        return Response({'message': 'Success'}, status=status.HTTP_200_OK)

    return Response({'message': bid_serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

