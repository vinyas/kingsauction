from rest_framework import serializers
from auction.users.models import User
from auction.auction_items.models import AuctionItem
from .models import Bid


class BidSerializer(serializers.ModelSerializer):
    bidder_id = serializers.SlugRelatedField(slug_field='uuid', source='bidder',
                                             queryset=User.objects.all())
    item_id = serializers.SlugRelatedField(slug_field='uuid', source='item',
                                           queryset=AuctionItem.objects.all())

    class Meta:
        model = Bid
        fields = ('bidder_id', 'item_id', 'amount')
