from django.apps import AppConfig


class BidsConfig(AppConfig):
    name = 'auction.bids'
