from django.urls import path

from .views import show_all_bids, add_bid

app_name = "bids"
urlpatterns = [
    path("list/", view=show_all_bids, name="list"),
    path("add-bid/", view=add_bid, name="add_bid"),

]
